import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'kanzburo_admin.settings'
sys.path.append('/srv/www/sandix/kanzburo_admin')
import datetime

import django
django.setup()

from watchdog.observers.polling import PollingObserver
from watchdog.events import PatternMatchingEventHandler
import mailparser
import base64
import requests
from bills.models import Bill, Contract, Seller
from bills.parser import PdfBillParserBase, Ertelecom
from bills.utils import parse_bill, prepare_xml
from bills.tasks import load_payment_crm
from kanzburo_admin.utils import send_notification_email
from celery.exceptions import TimeoutError

PDF_DIR = 'bills/pdfs'


class FileSystemListener(PatternMatchingEventHandler):
    patterns = ['*.eml']

    # def on_task_complete(self, task, result):
    #     pass

    def on_created(self, event):
        print('[{}] file {} detected'.format(
            datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S"), event.src_path)
        )
        file_name = event.src_path
        mail = mailparser.parse_from_file(file_name)

        # Check if Ertelecom
        if 'from' in mail.mail and mail.mail['from'][0][1] == 'bills@fromdomru.ru' \
                and mail.mail['subject'].find('счет') > 0:
            mail_body = mail.mail['body']
            start_index = mail_body.find('https://lkb2b.domru.ru')
            sub_string = mail_body[start_index:]
            href = sub_string[:sub_string.find("\">")]
            response = requests.get(href)
            if response.status_code == 200:
                f = open(os.path.join(PDF_DIR, 'ertelecom_bill.pdf'), 'wb')
                f.write(response.content)
                f.close()
                cls = Ertelecom
                self.create_bill(cls, f)
        else:
            # Standard PDF
            if mail.attachments:
                for att in mail.attachments:
                    if '.pdf' not in att['filename']:
                        continue

                    # Проверяем соответствует ли вложение критериям отбора файлов.
                    cls = PdfBillParserBase.get_parser_class(att['filename'], mail=mail)

                    if not cls:
                        print('parser class is not defined')
                        continue

                    print('cls: {}'.format(cls))

                    a = base64.b64decode(att['payload'])
                    f = open(os.path.join(PDF_DIR, att['filename']), 'wb')
                    f.write(a)
                    f.close()

                    self.create_bill(cls, f)

    @staticmethod
    def create_bill(cls, f):
        if not cls or not f:
            return

        bill = parse_bill(cls, file_path=f.name)
        os.remove(f.name)

        if not bill:
            print('Error parsing bill')
            return

        if Bill.objects.filter(name=bill.bill).exists():
            print('Bill is already exist')
            return

        seller, _ = Seller.objects.get_or_create(seller=bill.seller)
        contract, _ = Contract.objects.get_or_create(name=bill.contract_number, seller=seller)

        b_model = Bill(
            name=bill.bill,
            category=bill.category,
            contract=contract,
            buyer=bill.buyer,
            period=bill.period,
            sum=bill.sum,
            services=bill.services,
        )
        b_model.save()

        bill_xml = prepare_xml(b_model)
        res = load_payment_crm.delay({'bill_xml': bill_xml, 'bill_id': b_model.id})
        # res.get(callback=self.on_task_complete, propagate=True)
        try:
            payment = res.get(propagate=True, timeout=60)
            if payment != 0:
                b_model.payment_application_1c = payment
                b_model.save(update_fields=['payment_application_1c', ])
                send_notification_email(
                    'bills/payment_created_email.html',
                    b_model,
                    subject='Новая заявка расходования средств'
                )
        except TimeoutError:
            print('CRM answer timeout error')


observer = PollingObserver()
event_handler = FileSystemListener()
observer.schedule(event_handler, 'bills/mail/')

observer.start()
print('PollingObserver started')
observer.join()
