from bills.models import Bill
from kanzburo_admin.celery import app
from django.conf import settings
from zeep import Client as zeepClient


@app.task
def load_payment_crm(kwargs):
    """Загружает заявку на расходование средств в crm"""
    wsdl = getattr(settings, 'WSDL_ADDRESS', None)
    client = zeepClient(wsdl=wsdl)
    crm_payment_num = client.service.LoadZayavka(kwargs['bill_xml'])
    return crm_payment_num
