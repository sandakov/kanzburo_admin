from django.contrib import admin
from django.utils.formats import date_format

from bills.forms import BillInlineForm
from bills.models import Category, Contract, Seller, Bill


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'display')


class BillInlineAdmin(admin.TabularInline):
    model = Bill
    fields = ('created_at', 'name', 'sum', 'paid')
    readonly_fields = ('created_at', )
    extra = 0
    form = BillInlineForm

    # def get_readonly_fields(self, request, obj=None):
    #     if obj.paid:
    #         return self.readonly_fields + ('name', 'sum', 'paid')
    #     else:
    #         return self.readonly_fields

    def get_period(self, obj):
        if not obj.period:
            return ''
        return date_format(obj.period, format='F', use_l10n=True)


@admin.register(Contract)
class ContractAdmin(admin.ModelAdmin):
    list_display = ('name', 'account_number', 'display', 'active', 'desc')
    inlines = (BillInlineAdmin,)




@admin.register(Seller)
class SellerAdmin(admin.ModelAdmin):
    list_display = ('seller', 'code_1c')



@admin.register(Bill)
class BillAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'name', 'category', 'paid', 'sum', 'get_seller', 'get_period')

    def get_seller(self, obj):
        return obj.contract.seller

    def get_period(self, obj):
        if not obj.period:
            return ''
        return date_format(obj.period, format='F', use_l10n=True)

