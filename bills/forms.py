from django import forms

from bills.models import Bill


class BillInlineForm(forms.ModelForm):
    model = Bill

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'paid' in self.initial and self.initial['paid']:
            for fld in self.fields:
                self.fields[fld].disabled = True
