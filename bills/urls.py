# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import BillDetailView, BillListView

urlpatterns = [
    url(
        r'^$',
        BillListView.as_view(),
        name='bill_list'
    ),
    url(
        r'^(?P<pk>\d+)/$',
        BillDetailView.as_view(),
        name='bill_detail'
    )
]
