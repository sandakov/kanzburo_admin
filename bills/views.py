# -*- coding: utf-8 -*-
import pdftotext

from django.views.generic import ListView, DetailView, TemplateView

from bills.models import Bill
from bills.parser import RT, TTK, ZlatTK, PagerCom, PdfBillParserBase
from bills.tasks import load_payment_crm
from kanzburo_admin.utils import send_notification_email
from .utils import parse_bill, prepare_xml


def on_task_complete(obj, result):
    pass


class BillDetailView(DetailView):

    def get_object(self, queryset=None):
        return Bill.objects.get(id=self.kwargs['pk'])


class BillListView(ListView):
    paginate_by = 10

    def get_queryset(self):
        return Bill.objects.all().order_by('-id')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['parsed_data'] = self._get_parsed_data()
        return context

    @staticmethod
    def _get_parsed_data():
        # parser = PagerCom(pdf='bills/pdfs/Счет на оплату № ЦБ-1684 от 27 сентября 2018 г..pdf')
        # parser = RT(pdf='bills/pdfs/aug_2018_574000749562.pdf')
        # parser = ZlatTK(pdf='parsers/pdfs/bill.pdf')
        # bill = parser.parse()
        # f = 'bills/pdfs/aug_2018_574000749562.pdf'
        # f = 'bills/pdfs/aug_2018_574001746326.pdf'
        # f = 'bills/pdfs/6601-1808CH122186_201808_bill.pdf'
        # f = 'bills/pdfs/6601-1808CH146316_201808_bill.pdf'
        # f = 'bills/pdfs/schet_fop_20475_922599_184_30465461_1.pdf'
        # f = 'bills/pdfs/Счет на оплату № ЦБ-1684 от 27 сентября 2018 г..pdf'
        # f = 'bills/pdfs/Счет на оплату № ЦБ-1714 от 2 октября 2018 г..pdf'
        # f = 'bills/pdfs/6601-1809CH122186_201809_bill.pdf'
        f = 'bills/pdfs/6601-1809CH144578_201809_bill.pdf'
        # f = 'bills/pdfs/6601-1809CH146316_201809_bill.pdf'
        cls = PdfBillParserBase.get_parser_class(f)
        bill = parse_bill(cls, file_path=f)
        send_notification_email('bills/payment_created_email.html', bill)

        # bill_xml = prepare_xml(bill)
        # if not bill.payment_application_1c:
        #     res = load_payment_crm.delay({'bill_xml': bill_xml, 'bill_id': bill.id})
        #     payment_num = res.get(callback=on_task_complete, propagate=True)
        #     print(f'payment is: {res}')
        return
