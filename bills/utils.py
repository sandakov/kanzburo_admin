from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from xml.etree import ElementTree as ET


def parse_bill(cls, file_path=None):

    if not file_path:
        return
    parser = cls(pdf=file_path)
    bill = parser.parse()

    return bill


def prepare_xml(bill):
    root = ET.Element('root')

    seller = ET.SubElement(root, 'seller')
    seller.text = str(bill.contract.seller.code_1c)

    buyer = ET.SubElement(root, 'buyer')
    buyer.text = str(bill.buyer.code_1c)

    pay_date = ET.SubElement(root, 'pay_date')
    pay_date.text = str(bill.payment_date.strftime('%Y-%m-%d'))

    budget_category = ET.SubElement(root, 'budget_category')
    budget_category.text = bill.category.code_1c

    desc = ET.SubElement(root, 'desc')
    services = '\n'.join([service[0] for service in bill.services])
    desc.text = '{}\n{}'.format(bill, services)

    manager = ET.SubElement(root, 'manager')
    manager.text = 'Сандаков'

    pay_period = ET.SubElement(root, 'period')
    pay_period.text = str(bill.period.strftime('%Y-%m-%d'))

    sum = ET.SubElement(root, 'sum')
    sum.text = str(bill.sum)

    xml = ET.tostring(root, encoding='utf8').decode('utf8')

    return xml


def get_per_page(items, page=None, per_page=None):
    if not page:
        page = 1
    if not per_page:
        per_page = 3

    # https://docs.djangoproject.com/en/1.10/topics/pagination/
    paginator = Paginator(items, per_page)
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        items = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        items = paginator.page(paginator.num_pages)
    return items