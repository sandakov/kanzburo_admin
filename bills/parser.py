import pdftotext
from dateutil.relativedelta import relativedelta

import re

from bills.models import Category, Contract
from kanzburo_admin.models import Organization
import dateparser


class PdfBillParserBase(object):
    sep = ":"
    category = ''

    def __init__(self, pdf):
        super().__init__()
        with open(pdf, 'rb') as pdf:
            self.doc = pdftotext.PDF(pdf)[0]

        self.category, _ = Category.objects.get_or_create(name=self.get_category_name())
        self.contract_number = ''
        self.account_number = ''
        self.bill = ''
        self.seller = ''
        self.buyer = ''
        self.sum = ''
        self.period = ''
        self.services = []

    def get_category_name(self):
        raise NotImplementedError

    def get_string(self, pattern):
        """
        Стандартная функция парсинга по шаблону (pattern: need_value\n)
        :param pattern: паттерн по которому нужно производить поиск
        :return: Строка значения, идущая за следом за паттерном
        """

        start_index = self.doc.find(pattern)
        end_index = self.doc.find('\n', start_index)
        tmp_str = self.doc[start_index:end_index]
        sep_index = tmp_str.find(self.sep)
        if sep_index:
            tmp_str = tmp_str[sep_index + 1:]
        result = re.match('([ ]*)([\S ]+)', tmp_str)
        if not hasattr(result, 'groups'):
            return

        tmp_str = result.groups()[1]
        return tmp_str

    def get_string_replace(self, search_pattern, replace_content: list = None):
        """
        :param search_pattern: String which was need to be found
        :param replace_content: is a list of tuple. regex and content_replace_with
        :return: result string
        """
        result = self.get_string(search_pattern)
        result = re.sub(f'{search_pattern}', '', result)
        if not result:
            return

        if replace_content:
            for replace_row in replace_content:
                assert isinstance(replace_row, tuple)
                result = re.sub(f'{replace_row[0]}', replace_row[1], result)
        return result

    def string_parse(self, patter):
        """
        Возращает строку по шаблону pattern need_value\n
        :param patter:
        :return:
        """

    def parse(self):
        self.bill = self.get_bill()
        if not self.bill:
            return False
        self.contract_number, self.account_number = self.get_contract_info()
        self.seller = self.get_seller()
        self.buyer = self.get_buyer()
        self.period = self.get_period()
        self.services = self.get_services()
        self.sum = self.get_sum()
        return self

    def get_bill(self):
        raise NotImplementedError

    def get_seller(self):
        raise NotImplementedError

    def get_buyer(self):
        raise NotImplementedError

    def get_period(self):
        if not self.bill:
            return ''
        start_index = self.bill.find('от')
        date_str = self.bill[start_index + 3:]

        return dateparser.parse(date_str).replace(day=1)

    def get_services(self):
        raise NotImplementedError

    def get_sum(self):
        raise NotImplementedError

    def get_contract_info(self):
        raise NotImplementedError

    @staticmethod
    def get_parser_class(bill_name, mail=None):
        if bill_name.find('ЦБ-') > -1 and \
                '@pagercom.ru' in mail.mail['from'][0][1]:  # PagerCom
            return PagerCom
        elif any(
                [bill_name.find(i) > -1 for i in Contract.objects.filter(seller__seller__icontains='Ростелеком')
                    .values_list('account_number', flat=True)
                    .exclude(account_number__isnull=True)]
        ):  # RT
            return RT
        elif bill_name.find('6601-') > -1 and bill_name.find('bill') > -1:
            return TTK
        elif bill_name.find('schet_fop') > -1:
            return KTTK
        elif mail.subject.find('Отчетно  - платежные документы за ') > -1:
            return Intersvyaz
        elif bill_name.find('124933457_') > -1 and \
                mail.mail['from'][0][1] == 'manager@convex.ru':
            return Convex
        return ''


class RT(PdfBillParserBase):

    def get_category_name(self):
        return 'ГТС'

    def get_bill(self):
        return self.get_string_replace('Счет № ')

    def get_seller(self):
        return self.get_string_replace('Продавец:', replace_content=[('[»«]', '\"'), ])
        # return self.get_string('').replace('»', '\"').replace('«', '\"')

    def get_buyer(self):
        buyer = self.get_string('Покупатель:')
        return Organization.objects.filter(name__icontains=buyer).first()

    def get_services(self):
        tmp_str = self.doc[self.doc.rfind('Сумма'): self.doc.find('Итого начислено по счету')]
        tmp_str = tmp_str[tmp_str.find(' '):]
        tmp_str = re.split('[ ]{3,}', tmp_str)  # Убираем пробелы
        tmp_str.remove(tmp_str[0])  # Удаляем по краям пустые эл-ты, сделанные из пробелов.
        tmp_str.remove(tmp_str[len(tmp_str) - 1])
        bulk = 4
        services = []
        if len(tmp_str) % bulk == 0:
            chunks = int(len(tmp_str) / bulk)
            for chunk in range(0, chunks):
                idx = chunk * bulk
                services.append(
                    (tmp_str[idx], tmp_str[idx + 1], tmp_str[idx + 2], tmp_str[idx + 3][:-1].replace('-', '.')))
        return services

    def get_sum(self):
        start_index = self.doc.find('Итого к оплате по счету')
        end_index = self.doc.find('\n', start_index)
        tmp_str = self.doc[start_index:end_index]
        return tmp_str[tmp_str.rfind(' '):].replace('-', '.')

    def get_contract_info(self):
        start_index = self.doc.find('Договор')
        end_index = self.doc.find('\n', start_index)
        tmp_str = self.doc[start_index: end_index]
        info = tmp_str.split(',')
        contract_number = info[0][info[0].find('№'):]
        account_number = info[1][5:]
        return contract_number.replace('№ ', ''), account_number


class TTK(PdfBillParserBase):
    def get_category_name(self):
        return 'МГТС'

    def get_bill(self):
        return self.get_string_replace('Счет на оплату №')

    def get_seller(self):
        # return self.get_string('Продавец:')
        return 'ООО \"ТрансТелеком\"'

    def get_buyer(self):
        buyer = self.get_string('Покупатель:')
        return Organization.objects.filter(name=buyer).first()

    def get_services(self):
        tmp_str = self.doc[self.doc.rfind('Сумма, руб.'): self.doc.find('Итого:')]
        tmp_str = tmp_str[tmp_str.find('\n'):][3:]
        tmp_str = re.split('[ ]{3,}', tmp_str)  # Убираем пробелы
        tmp_str.remove(tmp_str[len(tmp_str) - 1])
        bulk = 2
        services = []
        if len(tmp_str) % bulk == 0:
            chunks = int(len(tmp_str) / bulk)
            for chunk in range(0, chunks):
                idx = chunk * bulk
                services.append(
                    (tmp_str[idx], re.sub(r'[\n руб]+', '', tmp_str[idx + 1][:-1]).replace(',', '.'))
                )
        return services

    def get_sum(self):
        return re.sub(r'[\n руб]+', '', self.get_string('Всего к оплате с НДС:')).replace(',', '.')

    def get_contract_info(self):
        pattern = 'Договор № '
        start_index = self.doc.find(pattern)
        end_index = self.doc.find('Период предоставления', start_index)
        tmp_str = self.doc[start_index: end_index]
        tmp_str = re.sub(r'[ ]{2,}', '', tmp_str)
        return tmp_str.replace(pattern, ''), ''


class ZlatTK(PdfBillParserBase):

    def get_bill(self):
        pass

    def get_seller(self):
        pass

    def get_buyer(self):
        pass

    def get_services(self):
        pass

    def get_sum(self):
        pass

    def get_contract_info(self):
        pass


class PagerCom(PdfBillParserBase):

    def get_category_name(self):
        return 'Принтеры'

    def get_bill(self):
        return self.get_string_replace('Счет на оплату № ')
        # return self.get_string('Счет на оплату').replace('Счет на оплату № ', '')

    def get_seller(self):
        # return self.get_string('Поставщик:')
        return 'СЦ \"Технологии\"'

    def get_buyer(self):
        return Organization.objects.get(name='ООО \"Канцбюро\"')

    def get_services(self):
        start_index = self.doc.find('Сумма')
        end_index = self.doc.find('Итого')
        tmp_str = self.doc[start_index: end_index]
        tmp_str = tmp_str[tmp_str.find('\n'):][3:]
        tmp_str = re.split('[ ]{3,}', tmp_str)  # Убираем пробелы

        if tmp_str[0] == '':
            tmp_str.remove(tmp_str[0])

        if tmp_str[len(tmp_str) - 1] == '':
            tmp_str.remove(tmp_str[len(tmp_str) - 1])

        bulk = 5
        services = []
        if len(tmp_str) % bulk == 0:
            chunks = int(len(tmp_str) / bulk)
            for chunk in range(0, chunks):
                idx = chunk * bulk
                services.append(
                    (
                        # tmp_str[idx],
                        tmp_str[idx + 1],
                        tmp_str[idx + 2],
                        re.sub(r'[\n руб.]+', '', tmp_str[idx + 3]).replace(',', '.'),
                        re.sub(r'[\n руб.]+', '', tmp_str[idx + 4]).replace(',', '.')
                    )
                )
        return services

    def get_sum(self):
        return re.sub(r'[\n руб.]+', '', self.get_string('Итого')).replace(',', '.')

    def get_contract_info(self):
        # title = 'Договор на обслуживание '
        # return self.get_string(title).replace(title, ''), ''
        return '120 от 09.01.2018', ''


class KTTK(PdfBillParserBase):

    def get_category_name(self):
        return 'ГТС'

    def get_bill(self):
        pattern = 'Счет на оплату № '
        return self.get_string_replace(pattern)
        # return self.get_string(pattern).replace(pattern, '')

    def get_seller(self):
        return 'АО \"Компания ТрансТелеКом\"'

    def get_buyer(self):
        return Organization.objects.get(name='ООО \"Канцбюро Трейд\"')

    def get_services(self):
        start_index = self.doc.find('Сумма')
        end_index = self.doc.find('Итого')
        tmp_str = self.doc[start_index: end_index]
        tmp_str = tmp_str[tmp_str.find('\n'):][3:]
        tmp_str = re.split('[ ]{3,}', tmp_str)  # Убираем пробелы

        if tmp_str[0] == '1':
            tmp_str.remove(tmp_str[0])

        if tmp_str[len(tmp_str) - 1] == '':
            tmp_str.remove(tmp_str[len(tmp_str) - 1])

        step = 5
        idx = 0

        while idx < len(tmp_str):
            tmp_str[idx] = '{} {}'.format(tmp_str[idx], tmp_str[idx + step][:-4])
            tmp_str.remove(tmp_str[idx + step])
            idx += step

        bulk = 5
        services = []
        if len(tmp_str) % bulk == 0:
            chunks = int(len(tmp_str) / bulk)
            for chunk in range(0, chunks):
                idx = chunk * bulk
                services.append(
                    (
                        tmp_str[idx],
                        tmp_str[idx + 1],
                        tmp_str[idx + 2],
                        re.sub(r'[\n руб.]+', '', tmp_str[idx + 3]).replace(',', '.'),
                        re.sub(r'[\n руб.]+', '', tmp_str[idx + 4]).replace(',', '.')
                    )
                )
        return services

    def get_sum(self):
        pattern = 'Всего:'
        return self.get_string(pattern).replace(pattern, '')

    def get_contract_info(self):
        contract_pattern = "Договор:"
        account_pattern = "Лицевой счёт:"
        return (
            self.get_string(contract_pattern).replace(contract_pattern, ''),
            self.get_string(account_pattern).replace(account_pattern, '')
        )


class Intersvyaz(PdfBillParserBase):

    def get_category_name(self):
        return 'Интернет'

    def get_bill(self):
        return self.get_string_replace('Счет № ')

    def get_seller(self):
        return 'ЗАО \"Интерсвязь-2\"'

    def get_buyer(self):
        return Organization.objects.filter(name='ООО \"Канцбюро Трейд\"').first()

    def get_services(self):
        start_index = self.doc.find('чество')
        end_index = self.doc.find('Итого')
        tmp_str = self.doc[start_index: end_index]
        tmp_str = tmp_str[tmp_str.find('\n'):][3:]
        tmp_str = re.split('[ ]{3,}', tmp_str)  # Убираем пробелы

        if tmp_str[0] == '':
            tmp_str.remove(tmp_str[0])

        if tmp_str[len(tmp_str) - 1] == '':
            tmp_str.remove(tmp_str[len(tmp_str) - 1])

        bulk = 5
        services = []
        if len(tmp_str) % bulk == 0:
            chunks = int(len(tmp_str) / bulk)
            for chunk in range(0, chunks):
                idx = chunk * bulk
                services.append(
                    (
                        tmp_str[idx][2:],
                        tmp_str[idx + 1],
                        tmp_str[idx + 2],
                        re.sub(r'[\n руб.]+', '', tmp_str[idx + 3]).replace(',', '.'),
                        re.sub(r'[\n руб.]+', '', tmp_str[idx + 4]).replace(',', '.')
                    )
                )
        return services

    def get_sum(self):
        pattern = 'Всего к оплате:'
        replace_content = [
            (',', '.')
        ]
        return float(self.get_string_replace(pattern, replace_content=replace_content))

    def get_contract_info(self):
        return '2504-2015/V', '8928178'


class Ertelecom(PdfBillParserBase):
    def get_category_name(self):
        return 'Интернет'

    def get_bill(self):
        bill_body = self.get_string_replace('СЧЕТ № ')
        end_index = bill_body.find('за')-1
        return bill_body[:end_index]

    def get_seller(self):
        return 'АО \"ЭР-Телеком Холдинг\"'

    def get_buyer(self):
        return Organization.objects.filter(name='ООО \"Канцбюро Трейд\"').first()

    def get_services(self):
        start_index = self.doc.find('НДС*       (руб.)')
        end_index = self.doc.find('ниченн')
        tmp_str = self.doc[start_index: end_index]
        tmp_str = tmp_str[tmp_str.find('\n'):][3:]
        tmp_str = re.split('[ ]{3,}', tmp_str)  # Убираем пробелы

        if tmp_str[0] == '':
            tmp_str.remove(tmp_str[0])

        if tmp_str[len(tmp_str) - 1] == '':
            tmp_str.remove(tmp_str[len(tmp_str) - 1])

        bulk = 5
        services = []
        if len(tmp_str) % bulk == 0:
            chunks = int(len(tmp_str) / bulk)
            for chunk in range(0, chunks):
                idx = chunk * bulk
                services.append(
                    (
                        tmp_str[idx],
                        re.sub(r'[\n руб.]+', '', tmp_str[idx + 2][:-4]).replace(',', '.'),
                        re.sub(r'[\n руб.]+', '', tmp_str[idx + 3]).replace(',', '.'),
                        re.sub(r'[\n руб.]+', '', tmp_str[idx + 4]).replace(',', '.')
                    )
                )
        return services

    def get_sum(self):
        replace_content = [
            ('в том числе НДС\*', ''),
            (' ', ''),
            (',', '.')
        ]
        return float(
            self.get_string_replace(
                'Общая сумма к оплате в текущем месяце:',
                replace_content=replace_content
            )
        )

    def get_contract_info(self):
        contract_pattern = 'ДОГОВОР № '
        account_pattern = 'л/с '
        replace_content = [('\)', '')]
        return \
            self.get_string_replace(contract_pattern),\
            self.get_string_replace(account_pattern, replace_content=replace_content)

    def get_period(self):
        if not self.bill:
            return ''
        start_index = self.bill.find('от')
        date_str = self.bill[start_index + 3:]

        return dateparser.parse(date_str, languages=['ru']).replace(day=1) - relativedelta(months=1)


class Convex(PdfBillParserBase):
    def get_category_name(self):
        return 'Интернет'

    def get_bill(self):
        result = self.get_string_replace('Счет № ')
        bill = self.get_string('Счет')
        if not bill:
            return
        return bill.replace('Счет № ', '')

    def get_seller(self):
        return 'ООО \"НТЦ Интек\"'

    def get_buyer(self):
        return Organization.objects.filter(name='ООО \"Канцбюро Трейд\"').first()

    def get_services(self):
        start_index = self.doc.find('Всего')
        end_index = self.doc.find('Итого')
        tmp_str = self.doc[start_index: end_index]
        tmp_str = tmp_str[tmp_str.find('\n'):][3:]
        tmp_str = re.split('[ ]{3,}', tmp_str)  # Убираем пробелы

        if tmp_str[0] == '':
            tmp_str.remove(tmp_str[0])

        if tmp_str[len(tmp_str) - 1] == '':
            tmp_str.remove(tmp_str[len(tmp_str) - 1])

        bulk = 7
        services = []
        if len(tmp_str) % bulk == 0:
            chunks = int(len(tmp_str) / bulk)
            for chunk in range(0, chunks):
                idx = chunk * bulk
                services.append(
                    (
                        tmp_str[idx],
                        re.sub(r"[\n]+", '', tmp_str[idx + 1]).replace(',', '.'),
                        re.sub(r"[\'\n]+", '', tmp_str[idx + 3]).replace(',', '.'),
                        re.sub(r"[\'\n]+", '', tmp_str[idx + 6]).replace(',', '.')
                    )
                )
        return services

    def get_sum(self):
        return float(3540)

    def get_contract_info(self):
        return 'И-825/14 от 27.05.2014', '124933457'

    def get_period(self):
        if not self.bill:
            return ''
        start_index = self.bill.find('от')
        date_str = self.bill[start_index + 3:]

        return dateparser.parse(date_str, languages=['ru']).replace(day=1) + relativedelta(months=1)
