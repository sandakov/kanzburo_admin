from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import JSONField
from datetime import datetime, timedelta
from workalendar.europe import Russia

work_cal = Russia()


class Bill(models.Model):
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    name = models.CharField(
        verbose_name='Счет',
        max_length=100,
        blank=True,
        null=True,
    )
    category = models.ForeignKey(
        'Category',
        verbose_name='Категория',
        blank=True,
        null=True,
        on_delete=models.CASCADE
    )
    contract = models.ForeignKey(
        'Contract',
        verbose_name='Договор',
        on_delete=models.CASCADE
    )
    buyer = models.ForeignKey(
        'kanzburo_admin.Organization',
        verbose_name='Покупатель',
        on_delete=models.CASCADE
    )
    period = models.DateField(
        verbose_name='Платежный период',
        blank=True,
        null=True,
    )
    sum = models.FloatField(
        verbose_name='Сумма оплаты',
        max_length=100,
        default=0
    )
    services = JSONField(
        verbose_name='Услуги',
        null=True,
        blank=True,
    )
    paid = models.BooleanField(
        verbose_name='Оплачено',
        default=False
    )
    payment_application_1c = models.CharField(
        verbose_name='Заявка в 1С',
        max_length=12,
        blank=True,
        null=True,
    )
    has_invoice = models.BooleanField(
        verbose_name='Закрывающий документ',
        default=False
    )
    payment_date = models.DateField(
        verbose_name='Планируемая дата расхода',
        blank=True,
        null=True
    )

    def __str__(self):
        return 'Счет № {}'.format(self.name)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        days_shift = getattr(settings, 'PAYMENT_DAY_SHIFT', 2)
        self.payment_date = datetime.today() + timedelta(days=days_shift)

        while work_cal.is_holiday(self.payment_date) or \
                not work_cal.is_working_day(self.payment_date):

            self.payment_date += timedelta(days=1)

        super().save(force_insert, force_update, using, update_fields)


class Seller(models.Model):

    seller = models.CharField(
        verbose_name='Продавец',
        max_length=100,
        blank=True,
        null=True,
    )

    code_1c = models.PositiveIntegerField(
        verbose_name='Код 1С',
        default=0
    )

    def __str__(self):
        return self.seller


class Contract(models.Model):

    name = models.CharField(
        verbose_name='Договор',
        max_length=100
    )
    seller = models.ForeignKey(
        'Seller',
        blank=True,
        null=True,
        verbose_name='Продавец',
        on_delete=models.CASCADE
    )
    account_number = models.CharField(
        verbose_name='Лицевой счет',
        max_length=100,
        blank=True,
        null=True,
    )
    desc = models.TextField(
        verbose_name='Описание',
        blank=True,
        null=True
    )
    active = models.BooleanField(
        verbose_name='Активен',
        default=True,
    )
    display = models.BooleanField(
        verbose_name='Отображать',
        default=True
    )

    def __str__(self):
        return 'Договор № {}'.format(self.name)


class Category(models.Model):

    name = models.CharField(
        verbose_name='Назваие категории',
        max_length=100,
        blank=True,
        null=True,
    )
    display = models.BooleanField(
        verbose_name='Отображать',
        default=True
    )

    code_1c = models.CharField(
        verbose_name='Код в 1с',
        max_length=20,
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name
