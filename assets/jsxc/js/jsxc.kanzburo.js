$(function() {
   jsxc.init({
      loginForm: {
         form: '#form',
         jid: '#username',
         pass: '#password'
      },
      logoutElement: $('#logout'),
      root: '/static/jsxc/',
      xmpp: {
         url: 'http://192.168.10.6:5280/http-bind/',
         domain: 'kanzburo.local',
         resource: 'office'
      }
   });
});

// jsxc.init({
//   loginForm: {
//     form: '#form',
//     jid: '#username',
//     pass: '#password'
//   },
//   logoutElement: $('#logout'),
//   loadSettings: function() {
//     return xmpp: {
//       url: '/http-bind/',
//       domain: 'localhost',
//       resource: 'example',
//       overwrite: true,
//       onlogin: true
//     };
//   },
//   root: '/jsxc/'
// });

$(document).on('ready.roster.jsxc', function(){
   $('#content').css('right', $('#jsxc_roster').outerWidth() + parseFloat($('#jsxc_roster').css('right')));
});
$(document).on('toggle.roster.jsxc', function(event, state, duration){
   $('#content').animate({
      right: ((state === 'shown') ? $('#jsxc_roster').outerWidth() : 0) + 'px'
   }, duration);
});