$(document).ready(function () {
    // var $dialog = $('#dialog');
    var $open_dialog_btn = $('#js-opendialog');
    var $close_dialog_btn = $('#js-closedialog');
    var $createuser_dialog_btn = $('#js-createuser');

    // $dialog.hide();

    $open_dialog_btn.click(function () {
        $.confirm({
            type: "red",
            // typeAnimated: true,
            // draggable: true,
            theme: "Light",
            title: "Удаление",
            content: "Вы действительно хотите удалить пользователя?",
            icon: "far fa-times-thin fa-2x",
            escapeKey: 'cancel',
            // animationBounce: 1.5,
            closeAnimation: 'zoom',
            backgroundDismiss: 'cancel',
            buttons: {
                yes: {
                    theme: "Dark",
                    text: "Да",
                    btnClass: "btn-red",
                    action: function () {
                        $.alert("Confirmed");
                    }
                },
                cancel: {
                    text: "Отмена"
                    // action: function () {
                    //     $.alert("Canceled");
                    // }
                }
            }
        })

    });

    $close_dialog_btn.click(function () {
       $dialog.hide();
    });

    $createuser_dialog_btn.click(function () {
        $.confirm({
            title: "Создание пользователя",
            content:''+
                '<form action="/" class="formName">' +
                '<div class="form-group">' +
                '<label>Enter something here</label>' +
                '<input type="text" placeholder="Your name" class="name form-control" required />' +
                '</div>' +
                '</form>',
            buttons: {
                ok: {
                    text: "OK",
                    action: function () {
                        var $form = this.$content.find('.formName');
                        $form.submit();
                    }
                },
                cancel: {
                    text: "Отмена"
                }
            }

        });
    })


    // function opendialog() {
    //     $dialog.fadeIn();
    // }
    //
    // function closedialog() {
    //     $dialog.fadeOut();
    // }

});

