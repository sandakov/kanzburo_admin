# -*- coding: utf-8 -*-

import django
django.setup()

import xlrd



# catalog_xls = pandas.read_excel('catalog.xls')
# catalog_xls.head()
# print catalog_xls[u'Код товара']
# print catalog_xls[u'Артикул']

xl_workbook = xlrd.open_workbook('catalog.xls')
xl_sheet = xl_workbook.sheet_by_index(0)

result = []
num_cols = xl_sheet.ncols
for row_idx in range(0, xl_sheet.nrows):
    row = []
    for col_idx in range(0, num_cols):
        cell_obj = xl_sheet.cell(row_idx, col_idx)
        row.append(cell_obj.value)
    result.append(row)
print(result)

from folders.tasks import set_folder_sizes_task

# set_folder_sizes_task()




