# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import xlrd
from django.shortcuts import render
from django.views.generic import ListView, DetailView
from smb.SMBConnection import SMBConnection

from folders.models import Folder, FolderSize, ShareResource
import os
from datetime import datetime, timedelta
from django.utils import timezone
import pytz


def get_smb_conn(server, path):
    conn = SMBConnection(
        username='admin',
        password='E;tYbxtujYtYflj',
        domain='KANZBURO',
        my_name='webserv',
        remote_name=server,
        use_ntlm_v2=True,
        is_direct_tcp=True

    )
    conn.connect(server, 445)

    return conn


class FolderListView(ListView):
    template_name = 'folders/shares_list.html'

    def get_queryset(self):
        # folders = FolderSize.objects.filter(folder__share_resource=root_dir)
        return ShareResource.objects.all()


def get_size(conn, share, top):
    dirs, files = [], []
    folder_size = 0
    print('processing %s%s' % (share, top))
    if not isinstance(conn, SMBConnection):
        raise TypeError("SMBConnection required")
    try:
        names = conn.listPath(share, top)
    except:
        print('Can\'t open %s%s' % (share, top))
        return

    for name in names:
        try:
            if name.isDirectory:
                if name.filename not in [u'.', u'..']:
                    dirs.append(name.filename)
                    new_path = os.path.join(top, name.filename)
                    folder_size += get_size(conn, share, new_path)

            else:
                folder_size += name.file_size
        except:
            print('something went wrong: ' \
                  'names: %s, ' \
                  'top: %s, ' \
                  'name: %s ' \
                  '' % (names, top, name.filename))

    return folder_size

    # for name in dirs:
    #     new_path = os.path.join(top, name)
    #     for x in smbwalk(conn, share, new_path):
    #         yield x


class FolderDetailView(ListView):
    template_name = 'folders/folders_list.html'

    def get_queryset(self):
        server = self.kwargs['server']
        folder = self.kwargs['folder']
        res = ShareResource.objects.get(server=server, name=folder)
        return FolderSize.objects.filter(folder__share_resource=res)


class ParseXlsView(ListView):
    template_name = 'folders/parse_xls.html'

    def get_queryset(self):
        xl_workbook = xlrd.open_workbook('catalog.xls')
        xl_sheet = xl_workbook.sheet_by_index(0)

        result = []
        num_cols = xl_sheet.ncols
        for row_idx in range(0, xl_sheet.nrows):
            row = []
            for col_idx in range(0, num_cols):
                cell_obj = xl_sheet.cell(row_idx, col_idx)
                row.append(cell_obj.value)
            result.append(row)
        gen = (r for r in result)
        print(gen)
        return gen
