# -*- coding: utf-8 -*-
import pytz
from celery.task import task
import celery

from datetime import datetime, timedelta
from django.utils import timezone

from folders.models import FolderSize, ShareResource, Folder
from folders.views import get_smb_conn, get_size


def set_folder_sizes(share_resource):
    # root_dir, is_created = ShareResource.objects.get_or_create(server='ts1.kanzburo.local', name='users')
    conn = get_smb_conn(share_resource.server, share_resource.name)
    # folder_size = smbwalk(conn, 'users', '/')

    for dir in conn.listPath(share_resource.name, '/'):
        if dir.isDirectory:
            if not dir.filename in [u'.', u'..']:
                dirr, is_created = Folder.objects.get_or_create(
                    share_resource=share_resource,
                    name=dir.filename,
                    path=r'%s/%s/%s' % (share_resource.server, share_resource.name, dir.filename)
                )
                if dirr.display:
                    folder_size_q = FolderSize.objects.filter(
                        folder=dirr
                    ).order_by('-timestamp')
                    is_need_create = False
                    if folder_size_q:
                        folder_size_obj = folder_size_q[0]
                        period_date = datetime.now(tz=pytz.UTC) - timedelta(days=2)
                        if timezone.localtime(folder_size_obj.timestamp) < timezone.localtime(period_date):
                            is_need_create = True
                    else:
                        is_need_create = True
                    if is_need_create:
                        folder_size_obj = FolderSize(
                            folder=dirr,
                            size=get_size(conn, share_resource.name, "/%s" % dirr.name)
                        ).save()


# @task(ignore_result=True, max_retries=1, default_retry_delay=10)

@task()
def set_folder_sizes_task():
    shares = ShareResource.objects.all()
    for share in shares:
        print(u'processing %s:%s' % (share.server, share.name))
        set_folder_sizes(share)
    print('done')
