# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.contrib import admin

from folders.views import FolderListView, FolderDetailView

urlpatterns = [
url(
        r'(?P<server>[a-zA-Z0-9\.\-]+)/(?P<folder>[\S]+)/$',
        FolderDetailView.as_view(),
        name=u'folder_detail'
    ),
    url(
        r'^$',
        FolderListView.as_view(),
        name=u'folder_list'
    ),

]