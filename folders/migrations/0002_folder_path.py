# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-03-28 10:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('folders', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='folder',
            name='path',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='path'),
        ),
    ]
