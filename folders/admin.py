# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from folders.models import Folder, FolderSize, ShareResource


class FolderAdmin(admin.ModelAdmin):
    list_display = (
        'share_resource',
        'name',
        'path',
        'slug'
    )


admin.site.register(Folder, FolderAdmin)


class FolderSizeAdmin(admin.ModelAdmin):
    list_display = (
        'folder',
        'size',
        'timestamp'
    )


admin.site.register(FolderSize, FolderSizeAdmin)

class ShareResourceAdmin(admin.ModelAdmin):
    list_display = (
        'server',
        'name',
        'comment'
    )


admin.site.register(ShareResource, ShareResourceAdmin)