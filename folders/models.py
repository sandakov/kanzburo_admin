# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.urls import reverse
from mptt.models import MPTTModel
from django.utils.translation import ugettext_lazy as _
from autoslug import AutoSlugField


class ShareResource(models.Model):
    server = models.CharField(
        verbose_name=_(u'server'),
        max_length=200
    )

    name = models.CharField(
        verbose_name=_(u'share_name'),
        max_length=100

    )

    comment = models.TextField(
        verbose_name=_(u'comment'),
        blank=True,
        null=True
    )

    def __unicode__(self):
        return u'%s - %s' % (self.server, self.name)


class Folder(models.Model):

    # parent = TreeForeignKey(
    #     'self',
    #     verbose_name=_(u'folder'),
    #     blank=True,
    #     null=True
    # )

    share_resource = models.ForeignKey(
        ShareResource,
        null=True,
        blank=True,
        on_delete=models.CASCADE

    )

    name = models.CharField(
        max_length=255,
        verbose_name=_(u'folder_name')
    )

    # size = models.BigIntegerField(
    #     verbose_name=_(u'size'),
    #     blank=True,
    #     null=True
    # )

    path = models.CharField(
        max_length=500,
        blank=True,
        null=True,
        verbose_name=_(u'path')
    )

    slug = AutoSlugField(
        verbose_name=_(u'slug'),
        max_length=1000,
        populate_from='name',
        unique_with='name',
        always_update=False,
        editable=True,
        blank=True,
        null=True
    )

    display = models.BooleanField(
        verbose_name=_('display'),
        default=True
    )
    # tree = TreeManager()

    def __unicode__(self):
        return u'%s' % self.name

    def _get_url(self):
        url = self.slug
        return url

    # @models.permalink
    def get_absolute_url(self):
        # return 'folder:detail', (), {'url': self._get_url()}
        return reverse('folder:detail', kwargs={'url': self._get_url()})


class FolderSize(models.Model):

    folder = models.ForeignKey(
        Folder,
        on_delete=models.CASCADE
    )

    size = models.BigIntegerField(
        verbose_name=_(u'size'),
        blank=True,
        null=True
    )

    timestamp = models.DateTimeField(
        verbose_name=_(u'timestamp'),
        auto_now_add=True
    )





