from django.db import models


class Organization(models.Model):
    name = models.CharField(
        verbose_name='Организация',
        max_length=100
    )
    display = models.BooleanField(
        verbose_name='Отображать',
        default=True
    )

    # crm_link = models.CharField(
    #     verbose_name='Организация в CRM. Бинарная ссылка БД',
    #     blank=True,
    #     null=True,
    #     max_length=255
    # )

    code_1c = models.CharField(
        verbose_name='Код организации в 1С',
        blank=True,
        null=True,
        max_length=20
    )

    def __str__(self):
        return self.name
