from django.conf.urls import url, include

from django.contrib import admin
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings
from folders.views import ParseXlsView

urlpatterns = [

    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('accounts.urls'), name=u'account_list'),
    url(r'^folders/', include('folders.urls'), name=u'folder_list'),
    url(r'parse/xls/', ParseXlsView.as_view(), name='parse_xls'),
    url(r'bills/', include('bills.urls'), name='bills'),
    url(r'xmpp/', TemplateView.as_view(template_name='xmpp.html')),
    # url(r'xmpp/', include("xmpp.urls")),
    url(r'^$', TemplateView.as_view(template_name='index.html'), name=u'index'),


] \
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
