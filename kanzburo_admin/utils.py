from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.conf import settings


def send_notification_email(default_template, bill, subject='Оповещение системы управления Канцбюро'):
    context = {
     'bill': bill,
    }

    html_content = render_to_string(default_template, context)

    msg = EmailMessage(subject, html_content, settings.DEFAULT_FROM_EMAIL, settings.NOTIFIERS_EMAILS)
    msg.content_subtype = 'html'
    msg.send()
