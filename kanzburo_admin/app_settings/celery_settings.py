from celery.schedules import crontab
from datetime import timedelta

CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_TASK_RESULT_EXPIRES = 7 * 86400  # 7days
# CELERY_SEND_EVENTS = True
# CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'
CELERY_BEAT_SCHEDULE = {
    'set_folder_sizes_task': {
        'task': 'folders.tasks.set_folder_sizes_task',
        # 'schedule': crontab(minute=0, hour='*/3,10-19'),
        'schedule': crontab(hour='*/1'),
        # 'schedule': timedelta(seconds=10),
    }
}


# import djcelery
# djcelery.setup_loader()
