XMPP_DOMAIN = 'kanzburo.local'
XMPP_BOSH_SERVICE_URL = 'https://192.168.10.6:5280/http-bind'
XMPP_CONVERSEJS_SETTINGS = {
    'allow_contact_removal': False,
    'allow_contact_requests': True,
    'auto_subscribe': True,
    'allow_logout': False,
    'allow_muc': True,
    'allow_otr': False,
    'allow_registration': False,
    'message_carbons': True,
    'hide_muc_server': True,
    'use_vcards': True,
    'animate': True,
    'play_sounds': True,
    'xhr_user_search': True,
    'sounds_path': '%ssounds/' % '/static/',
    'visible_toolbar_buttons': {
         'call': False,
         'clear': False,
         'emoticons': True,
         'toggle_participants': False,
    }
}