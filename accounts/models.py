# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class AdAccount(models.Model):
    username = models.CharField(
        max_length=100,
        verbose_name=_(u'username')
    )
    displayName = models.CharField(
        max_length=100,
        verbose_name=_(u'displayName'),
        blank=True,
        null=True
    )
    lastLogonTimestamp = models.DateTimeField(
        verbose_name=_('lastLogonTimestamp'),
        blank=True,
        null=True
    )
    description = models.TextField(
        verbose_name=_(u'description'),
        blank=True,
        null=True
    )
    distinguishedName = models.CharField(
        max_length=500,
        verbose_name=_(u'distinguishedName')
    )
    userAccountControl = models.BigIntegerField(
        verbose_name=_(u'userAccountControl'),
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name=_(u'updated_at')
    )