# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, FormView, DetailView, UpdateView
from ldap3 import Connection, Writer, ObjectDef, Reader
from ldap3 import MODIFY_ADD, MODIFY_REPLACE, MODIFY_DELETE
# from ldap import LdapUserDisabler, LdapUserInfo



import dateutil.parser

from accounts.forms import LdapAccountForm
from accounts.models import AdAccount


def account_list():
    # 66050 - accountdisabled | normal_account | dont_expire_password
    # 66048 - normal_account | dont_expire_password

    conn = Connection('192.168.10.5', auto_bind=True, user=u'kanzburo\\admin', password=u'E;tYbxtujYtYflj')
    objDef = ObjectDef()
    # objDef.clear_attributes()
    objDef.add_attribute('lastLogon')
    objDef.add_attribute('lastLogonTimestamp')
    objDef.add_attribute('sAMAccountName')
    objDef.add_attribute('displayName')
    objDef.add_attribute('userAccountControl')

    r = Reader(
        conn,
        objDef,
        'dc=kanzburo,dc=local',
        '(&(objectCategory=person)(objectClass=user)(userAccountControl:1.2.840.113556.1.4.803:=2))',
        # '(&(objectCategory=person)(objectClass=user)(sAMAccountName=samson))',
        # attributes=['lastLogon', 'lastLogonTimestamp', 'sAMAccountName', 'displayName', 'userAccountControl']
    )
    result = r.search()
    # result = conn.search(
    #     'dc=kanzburo,dc=local',
    #     '(&(objectCategory=person)(objectClass=user)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))',
    #     attributes=['lastLogon', 'lastLogonTimestamp', 'sAMAccountName', 'displayName', 'userAccountControl'])
    if result:
        # w = Writer.from_cursor(r)
        # w.query_filter('(sAMAccountName=samson)')
        # w[75].userAccountControl = 66048

        # w.commit()
        # w = Writer.from_cursor(conn.entries[75].entry_cursor)
        # conn.modify(conn.entries[75].entry_dn, {'userAccountControl': [(MODIFY_ADD, ['66050'])]})
        return r.entries


class AccountList(ListView):
    template_name = 'accounts/account_list.html'

    def get_queryset(self):
        # conn = Connection('192.168.10.5', auto_bind=True, user=u'kanzburo\\admin', password=u'E;tYbxtujYtYflj')
        # objDef = ObjectDef()
        # # objDef.add_attribute('lastLogon')
        # objDef.add_attribute('lastLogonTimestamp')
        # objDef.add_attribute('sAMAccountName')
        # objDef.add_attribute('displayName')
        # objDef.add_attribute('userAccountControl')
        # objDef.add_attribute('description')
        # objDef.add_attribute('distinguishedName')
        # objDef.add_attribute('objectClass')
        #
        #
        # r = Reader(
        #     conn,
        #     objDef,
        #     'dc=kanzburo,dc=local',
        #     '(&(objectCategory=person)(objectClass=user)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))',
        # )
        # if r.search():
        #
        #     for entry in r.entries:
        #         try:
        #             ad_user = AdAccount.objects.get(username=entry.sAMAccountName)
        #             # print '%s: created: %s'% (ad_user.username, ad_user.updated_at)
        #         except:
        #             ad_user = AdAccount()
        #             ad_user.username = entry.sAMAccountName
        #             ad_user.description = entry.description
        #             if not str(entry.lastLogonTimestamp) == '[]':
        #                 ad_user.lastLogonTimestamp = dateutil.parser.parse(str(entry.lastLogonTimestamp))
        #             ad_user.displayName = entry.displayName
        #             ad_user.userAccountControl = long(entry.userAccountControl.value)
        #             ad_user.distinguishedName = entry.distinguishedName
        #
        #             ad_user.save()
        #     for item in AdAccount.objects.all():
        #         print '%s: %s' % (item.username, item.displayName)
        #     return r.entries
        return AdAccount.objects.all()

class AccountDetail(UpdateView):
    template_name = 'accounts/account_detail.html'
    form_class = LdapAccountForm
    success_url = reverse_lazy('account_list')
    attrs = ['sAMAccountName', 'userAccountControl', 'displayName', 'description']

    def form_valid(self, form):
        sAMAccountName = form.cleaned_data['sAMAccountName']
        disabled = form.cleaned_data['disabled']
        description = form.cleaned_data['description']

        # lconn = LdapUserDisabler(
        #     attrs=self.attrs,
        #     sAMAccountName=sAMAccountName,
        #     disabled=disabled,
        #     description=description
        # )
        # lconn.execute()
        return redirect(self.get_success_url())

    def get_object(self, queryset=None):
        pass
        # return LdapUserInfo(
        #     attrs=self.attrs,
        #     sAMAccountName=self.kwargs['account'],
        # ).execute()

    def get_form(self, form_class=None):
        if self.request.POST:
            form = self.form_class(self.request.POST)
        else:
            form = self.form_class(
                initial={
                    'sAMAccountName': self.object.sAMAccountName,
                    'displayName': self.object.displayName,
                    'disabled': True if self.object.userAccountControl == 66050 else False,
                    'description': self.object.description
                }
            )
        return form

