# -*- coding: utf-8 -*-
from django import forms


class LdapAccountForm(forms.Form):
    displayName = forms.CharField(
        max_length=50,
        required=False
    )
    sAMAccountName = forms.CharField(
        max_length=50,
        required=True
    )
    description = forms.CharField(
        max_length=100,
        required=False
    )
    disabled = forms.BooleanField(
        required=False
    )
