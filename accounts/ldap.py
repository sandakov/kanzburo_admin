# -*- coding: utf-8 -*-

from ldap3 import Connection, Writer, ObjectDef, Reader
from django.conf import settings


class LdapConnectorBase(object):

    def __init__(self, attrs=None, **kwargs):
        super(LdapConnectorBase, self).__init__()
        self.kwargs = kwargs
        self.ldap_base = getattr(settings, 'LDAP_BASE', '')
        self.connection = Connection(
            getattr(settings, 'LDAP_SERVER', '192.168.10.5'),
            auto_bind=True,
            user=getattr(settings, 'LDAP_USER', 'admin'),
            password=getattr(settings, 'LDAP_PASSWORD', 'None')
        )
        self.objDef = ObjectDef()
        if isinstance(attrs, list):
            for attr in attrs:
                self.objDef.add_attribute(attr)

    def execute(self):
        raise NotImplementedError


class LdapUserDisabler(LdapConnectorBase):
    # 66050 - accountdisabled | normal_account | dont_expire_password
    # 66048 - normal_account | dont_expire_password
    # https://support.microsoft.com/ru-ru/help/305144/how-to-use-the-useraccountcontrol-flags-to-manipulate-user-account-pro

    def execute(self):
        if 'sAMAccountName' in self.kwargs:
            r = Reader(
                self.connection,
                self.objDef,
                self.ldap_base,
                '(&(objectCategory=person)(objectClass=user)(sAMAccountName=%s))' % self.kwargs['sAMAccountName'],
            )
            result = r.search()
            if result:
                w = Writer.from_cursor(r)
                w[0].userAccountControl = 66050 if self.kwargs['disabled'] else 66048
                w.commit()
        else:
            raise NotImplementedError


class LdapUserInfo(LdapConnectorBase):

    def execute(self):
        if 'sAMAccountName' in self.kwargs:
            r = Reader(
                self.connection,
                self.objDef,
                self.ldap_base,
                '(&(objectCategory=person)(objectClass=user)(sAMAccountName=%s))' % self.kwargs['sAMAccountName'],
            )
            result = r.search()
            if result:
                return r[0]
        else:
            raise NotImplementedError

