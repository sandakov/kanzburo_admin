from django.conf.urls import url
from django.contrib import admin
from accounts.views import AccountList, AccountDetail

urlpatterns = [
url(
        r'edit/(?P<account>[\S]+)/$',
        AccountDetail.as_view(),
        name=u'account_detail'
    ),
    url(
        r'^$',
        AccountList.as_view(),
        name=u'account_list'
    ),

]